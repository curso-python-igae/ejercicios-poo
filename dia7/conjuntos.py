def es_primo(num):
    for n in range(2, num):
        if num % n == 0:
            return False
    return True

def get_pares(items=50):
    return [numero for numero in range(1,items*2+1) if numero % 2 == 0]

def get_impares(items=50):

    return [numero for numero in range(items*2+1) if numero % 2 != 0]

def get_primos(items=50):
    numeros = []
    i = 0
    while len(numeros) <= items:
        if (es_primo(i)):
            numeros.append(i)
        i += 1
    return numeros


items = 50 # Debe ser un entero o None
if items is not None:
    pares = frozenset(get_pares(items))
    impares = frozenset(get_impares(items))
    primos = frozenset(get_primos(items))
else:
    pares = frozenset(get_pares())
    impares = frozenset(get_impares())
    primos = frozenset(get_primos())

print("*** Genera las frozenset de núemros solicitados ***")
print(f"Pares: {pares}")
print(f"Impares: {impares}")
print(f"Numeros primos: {primos}")

print("*** Operaciones con los conjuntos ***")
print(f"Intersección de primos con pares: {primos.intersection(pares)}")
print(f"Intersección de primos con impares: {primos.intersection(impares)}")
print(f"Diferencia simétrica entre primos y pares {primos.symmetric_difference(pares)}")
print(f"Diferencia simétrica entre primos e impares {primos.symmetric_difference(impares)}")