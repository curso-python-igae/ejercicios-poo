class ReverseString:
    
    def __init__(self, frase):
        self.__frase = frase
        
    def get_frase_inversa(self):
        aux = self.__frase.split()
        aux.reverse()
        return " ".join(aux)
        # return ' '.join(self.__frase.split()[::-1]) Corrección de clase


frase_inversa = ReverseString("Mi diario Python")
print(frase_inversa.get_frase_inversa())

