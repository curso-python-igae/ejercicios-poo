class Rectangulo():

    def __init__(self, base=0, altura=0):
        self.__base = base
        self.__altura = altura

    def get_area(self):
        return self.__base * self.__altura


rectangulo = Rectangulo(4,5)
print(f"El área del rectángulo es {rectangulo.get_area()}")