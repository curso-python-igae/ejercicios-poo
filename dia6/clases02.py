
class GetString:

    def __init__(self, text=None):
        if text is not None:
            self.__frase = text

    def get_string(self, text: str):
        self.__frase = text

    def print_string(self):
        print(self.__frase.upper())


frase = GetString()
frase.get_string("Hola caracola")
frase.print_string()
