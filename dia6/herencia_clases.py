class Vehicle:

    def __init__(self, color: str = None, wheels: int = 4):
        self.color = color
        self.wheels = wheels

    def __str__(self):
        return "Este vehiculo es de color {} y tiene {} ruedas".format(self.color, self.wheels)


class Car(Vehicle):

    def __init__(self, color: str = None, wheels: int = 4, speed: int = 0, cilindrada: int = 0):
        super().__init__(color, wheels)
        self.speed = speed
        self.cilindrada = cilindrada

    def __str__(self):
        return ("Este vehiculo es de color {} y tiene {} ruedas. Alcanza los {} (km/h) y su cilindrada "
                "es de {} cc".format(self.color, self.wheels, self.speed, self.cilindrada))


class Van(Car):

    def __init__(self, color: str = None, wheels: int = 4, speed: int = 0, cilindrada: int = 0, carga: int = 0):
        super().__init__(color, wheels, speed, cilindrada)
        self.carga = carga

    def __str__(self):
        return ("Este vehiculo es de color {} y tiene {} ruedas. Alcanza los {} (km/h) y su cilindrada "
                "es de {} cc.\nSu capacidad de carga es de {} kilos".format(self.color, self.wheels, self.speed,
                                                                            self.cilindrada, self.carga))


class Bicycle(Vehicle):

    def __init__(self, color: str = None, wheels: int = 2, tipo: str = None):
        super().__init__(color, wheels)
        self.tipo = tipo

    def __str__(self):
        return ("Este vehiculo es de color {} y tiene {} ruedas. Es una bicicleta de tipo {}"
                .format(self.color, self.wheels, self.tipo))


class Motorbike(Bicycle):

    def __init__(self, color: str = None, wheels: int = 2, tipo: str = None, velocidad: int = 0, cilindrada: int = 0):
        super().__init__(color, wheels, tipo)
        self.velocidad = velocidad
        self.cilindrada = cilindrada

    def __str__(self):
        return ("Este vehiculo es de color {} y tiene {} ruedas. Es una {} de tipo {}."
                "\nAlcanza los {} (km/h) y su cilindrada es de {} cc."
                .format(self.color, self.wheels, type(self).__name__, self.tipo, self.velocidad, self.cilindrada))


def catalogar(vehicles: list):
    for v in vehicles:
        print(f"** Esto es un {type(v).__name__} **")
        print(v)


vehicle = []
standard_vehicle = Vehicle(color="rojo", wheels=4)
vehicle.append(standard_vehicle)
car = Car(color="verde", wheels=2, speed=120, cilindrada=900)
vehicle.append(car)
van = Van(color="verde", wheels=2, speed=120, cilindrada=900, carga=1200)
vehicle.append(van)
bicycle = Bicycle(color="verde", wheels=2, tipo="urbana")
vehicle.append(bicycle)
motorbike = Motorbike(color="verde", wheels=2, tipo="urbana", velocidad=220, cilindrada=1200)
vehicle.append(motorbike)

catalogar(vehicle)
